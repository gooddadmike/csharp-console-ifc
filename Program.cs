using System;
using System.Globalization;

namespace IFC
{
    public class Convert
    {
        public int dayOfYear { get; set; }
        public DateTime dateValue = DateTime.Now;
        public bool ifc { get; set; }
        public bool isLeapYear { get; set; }

        class Program
        {
            static void Main(string[] args)
            {
                Convert c = new Convert();
                c.GetArgs(args);
                if (c.ifc)
                {
                    c.IfcToGregorian(args[0]);
                }
                else
                {
                    c.GregorianToIfc(args[0]);
                }
            }
        }

        public void GetArgs(string[] args)
        {
            if (args != null && args.Length != 0)
            {
                if (args.Length == 2)
                {
                    ifc = string.Equals(args[1].ToLower(), "ifc");
                    if (!ifc)
                    {
                        ThrowErrorEnd("Second value, if provided must be a \"IFC\"");
                    }
                    else
                    {
                        return;
                    }
                }
            }

        }

        private static void ThrowErrorEnd(string msg)
        {
            try
            {
                throw new System.ArgumentException(msg);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
                Environment.Exit(0);
            }
        }

        public void GregorianToIfc(string dateArg)
        {
            int month = 12;
            int dayOfMonth = 28;
            int dayOfWeekNumber = 7;
            string dayOfWeek = "Year Day!";
            string[] monthNames = SetIfcMonthNames();
            SetDate(dateArg);

            // June 28th + 1 day is Leap Day on Leap Years
            bool isLeapDay = (isLeapYear && dayOfYear == 169);
            // December 31st is Year Day
            bool isYearDay = ((!isLeapYear && dayOfYear == 365) || (isLeapYear && dayOfYear == 366));
            // after leap day subtract a day from day of year
            if (isLeapYear && dayOfYear > 169) dayOfYear--;

            if (isLeapDay || isYearDay)
            {
                // dayOfMonth default value is 28, increment for Leap Day / Year Day
                dayOfMonth++;
                if (isLeapDay)
                {
                    month = 5;
                    dayOfWeek = "Leap Day!";
                }
            }
            else
            {
                // calculate the day of year
                month = dayOfYear / 28;
                if (dayOfYear % 28 == 0)
                {
                    // if (dayOfYear % 28 == 0) decrement month and use default date
                    month--;
                }
                else
                {
                    dayOfMonth = dayOfYear % 28;
                }

                if (dayOfMonth % 7 != 0) dayOfWeekNumber = dayOfMonth % 7; // use default or set dOWN = dOM % 7
                dayOfWeek = new DateTimeFormatInfo().DayNames[dayOfWeekNumber - 1];
            }

            dayOfWeek += ",";

            Console.WriteLine("IFC: " + dayOfWeek + " " + monthNames[month] + " " + dayOfMonth + ", " + dateValue.Year);
        }

        private string[] SetIfcMonthNames()
        {
            string[] monthNames = new string[13];
            monthNames[6] = "Sol";
            for (int i = 0; i < monthNames.Length; i++)
            {
                if (i < 6) monthNames[i] = new DateTimeFormatInfo().MonthNames[i];
                if (i > 6) monthNames[i] = new DateTimeFormatInfo().MonthNames[i - 1];
            }
            return monthNames;
        }

        private void SetDate(string dateStr)
        {
            bool inputIsDate = DateTime.TryParse(dateStr, out dateValue);
            if (!inputIsDate) ThrowErrorEnd("Use a valid a parseable date");

            dateValue = dateValue.ToUniversalTime();
            dayOfYear = dateValue.DayOfYear;
            isLeapYear = DateTime.IsLeapYear(dateValue.Year);
        }

        public void IfcToGregorian(string ifcDateValue)
        {
            int year;
            int month;
            int day;

            bool isYear = int.TryParse(ifcDateValue.Substring(0, 4), out year);
            bool isMonth = int.TryParse(ifcDateValue.Substring(5, 2), out month); ;
            bool isDay = int.TryParse(ifcDateValue.Substring(8, 2), out day);

            isLeapYear = DateTime.IsLeapYear(year);

            if (!isYear || !isMonth || !isDay ||
                    (day > 28 && month != 6 && month != 13) ||
                    (month == 13 && day > 29) ||
                    (month == 6 && ((isLeapYear && day > 29) || (!isLeapYear && day > 28)))
                ) ThrowErrorEnd("Invalid IFC value. Months 1-13; Days 1-28. Exceptions: Leap Day 6/29 & Year Day 12/29");
            dayOfYear = (((month - 1) * 28) + day);
            if (isLeapYear && dayOfYear >= 169 && month > 6) dayOfYear++; // day of year is one higher after Leap Day (6/29) 
            DateTime gregDate = new DateTime(year, 1, 1).AddDays(dayOfYear - 1);
            Console.WriteLine("Greg: " + gregDate.ToString("D"));
        }
    }
}
