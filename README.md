# International Fixed Calendar

[International Fixed Calendar](https://en.wikipedia.org/wiki/International_Fixed_Calendar) is an alternate and better calendar. There are 28 days in a month all starting on Sunday 1st and ending on Saturday 28th.

There are 13 months with maximum of two days that exists outside of weekdays (in a sort of limbo bewteen Saturday and Sunday). The extra month is called Sol and occurs between June and July. The extra days occur on Gregorian Calendar Dec 31st (Year Day) and day of year # 169 of any leap year (Leap Day). In the IFC this lands between June 28th and Sol 1st.

## But Why?

It's not the easiest thing to articulate but I often feel like things are just a few degrees from being much better than they could be in small and simple ways. Besides the IFC, I'm the kind of guy that once abandoned the QWERTY keyboard layout and learned to type in [Dvorak](https://www.dvzine.org/zine/). I'm planning on switching to [Norman](https://normanlayout.info) keyboard layout in the future. I keep a Zulu Time (Universal Coordinated Time) showing on my watch, and I tie my shoes with an [Ian Knot](https://www.fieggen.com/shoelace/ianknot.htm).  There are better ways and standards than we have been given and my love for IFC is just one of those and a nice place, I thought to start my #100DaysOfCode challenge.

My goal is to learn mobile development and to eventually get a widget view always available for showing IFC on my phone and other devices. I'm not commited to forcing my delusion on anyone else, I just like to keep a thread or two sometimes thinking in this parallel universe where things are better.